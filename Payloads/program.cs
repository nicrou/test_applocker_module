﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace TestingApplication {
  public class Program {
    public static void owning() {
      //C: \Users\UserName\AppData\Local\Temp\  
      string path = Path.GetTempPath() + @"\results.txt";

      if (!File.Exists(path)) {
        using (StreamWriter file = File.CreateText(path)) {
          file.WriteLine("PASSED!");
        }
      } else {
        using (StreamWriter file = File.AppendText(path)) {
          file.WriteLine("PASSED!");
        }
      }
    }
    static void Main(string[] args) {
      owning();
    }
  }
}