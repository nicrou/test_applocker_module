Function Get-FilePathRules {
  <#
  .SYNOPSIS
    This script lists default ACL for the "BUILTIN\users" group looking for write/createFiles & execute authorizations
  .DESCRIPTION
    One of the Default Rules in AppLocker allows everything in the folder C:\Windows to be executed.
    A normal user shouln't have write permission in that folder, but that is not always the case.
    This script lists default ACL for the "BUILTIN\users" group looking for write/createFiles & execute authorizations.
  .PARAMETER Group
    An optional paramameter that sets the group of users that it will test agains. By default it uses "*Users*"
  .PARAMETER RootFolder
    The folder from which it will start the recursive interations. It is optional and by default the "C:\Windows" will be used.
  .EXAMPLE
    Get-FilePathRules -RootFolder "C:\Windows\Tasks"
  .NOTES
    Change the group and root_folder variables to suit your needs
  #>

  param(
    [parameter(Mandatory=$false)]
    [string] $Group = "*Users*",
    [ValidateScript({Test-Path $_})]
    [parameter(Mandatory=$false)]
    [string] $RootFolder = "C:\Windows"
  )

  Write-Output "[*] Processing folders recursively in $RootFolder"
  foreach($_ in (Get-ChildItem $RootFolder -recurse -ErrorAction SilentlyContinue)) {
    if($_.PSIsContainer) {
      try {
        $res = Get-acl $_.FullName 
      } catch {
        continue
      }
      foreach ($a in $res.access) {
        if ($a.IdentityReference -like $Group) {
          if (($a.FileSystemRights -like "*Write*" -or $a.FileSystemRights -like "*CreateFiles*") -and $a.FileSystemRights -like "*ReadAndExecute*"){
            Write-Host "[+] " $_.FullName -foregroundcolor "green"
          }
        }
      }
    }
  }
}