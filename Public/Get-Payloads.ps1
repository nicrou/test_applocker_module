Function Get-Payloads {
  <#
  .SYNOPSIS
    Lists Module's payloads.
  .DESCRIPTION
    Lists Module's payload files (every file located at $Global:TestAppLockerRootPath\Payloads). 
  .EXAMPLE
    Get-Payloads
  #>

  # Get payload files.
  Get-ChildItem -Path "$Global:TestAppLockerRootPath\Payloads" -Recurse -File
}