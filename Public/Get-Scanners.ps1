Function Get-Scanners {
  <#
  .SYNOPSIS
    Lists Module's scanners.
  .DESCRIPTION
    Lists Module's payload files (every file located at $Global:TestAppLockerRootPath\Scanners). 
  .EXAMPLE
    Get-Scanners
  #>

  # Get scanner files.
  Get-ChildItem -Path "$Global:TestAppLockerRootPath\Scanners" -Recurse -File
}