Function Use-Scanner {
  <#
  .SYNOPSIS
    Invokes a scanner script.
  .DESCRIPTION
    Invokes a scanner scirpt. 
  .PARAMETER Scanner
    The absolute path to the scanner script or the name of an existing scanner located at $Global:TestAppLockerRootPath\Scanners. 
  .EXAMPLE
    Use-Scanner -Scanner SampleScanner.ps1
  #>

  param(
    [parameter(Mandatory=$true)]
    [string]$Scanner
  )

  if ((Test-Path $Scanner) -eq $false) {
    $Scanner = Search-File -FileName $Scanner -Type Scanner
  }

  &$Scanner
}