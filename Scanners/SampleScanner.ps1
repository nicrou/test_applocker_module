
$InfoDictionary = Get-Info
$Count = Get-Content (Join-Path $InfoDictionary['TempPath'] "results.txt") | Measure-Object -Line
Function evaluate($ByPass, $Count) {
  Start-Sleep -s 1
  $NCount = Get-Content (Join-Path $InfoDictionary['TempPath'] "results.txt") | Measure-Object -Line
  
  # TODO: Remove these.
  # Write-Host $Count.Lines
  # Write-Host $NCount.Lines

  if ($NCount.Lines -gt $Count.Lines) {
    Write-Host "[+] $ByPass - Passed." -fore green
  } else {
    Write-Host "[-] $ByPass - Failed." -fore red
  }
  
  return $NCount
}

##########  ====== Begin - Testing runtime .Net Core type definition from .dll via PowerShell
Write-Host '====== Begin - Testing runtime .Net Core type definition from .dll via PowerShell...'
Use-DotNetCoreType -Payload 'testingPayload.dll' -Class 'TestingPayload' -ScriptBlock { (Get-Variable -Name testingPayload -ValueOnly).owning() }
$Count = evaluate "Testing runtime .Net Core type definition from .dll via PowerShell" $Count
Write-Host '====== End'
##########  ====== End

##########  ====== Begin - Testing payload execution using builtin CL_LoadAssembly.ps1 script
Write-Host '====== Begin - Testing payload execution using builtin CL_LoadAssembly.ps1 script...'
Use-CLLoadAssembly -Payload 'testingPayload_csc_v2.exe' -ScriptBlock { [TestingApplication.Program]::owning() }
$Count = evaluate "Testing payload execution using builtin CL_LoadAssembly.ps1 script" $Count
Write-Host '====== End'
##########  ====== End

##########  ====== Begin - Testing payload execution using builtin InstalUtil.exe
Write-Host '====== Begin - Testing payload execution using builtin InstalUtil.exe...'
Use-InstallUtil -Payload 'installUtilPayload.exe'
$Count = evaluate "Testing payload execution using builtin InstalUtil.exe" $Count
Write-Host '====== End'
##########  ====== End