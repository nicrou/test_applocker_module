Function Set-PrivatePlaceholder {
  <#
  .SYNOPSIS
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  .DESCRIPTION
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nunc arcu, rutrum id leo et, feugiat ultricies dolor.
  .EXAMPLE
    
  .NOTES
    Functions under the Private folder are not Exported as Members of the Module but can be used internally. 
  #>

}