Function ConvertTo-Base64 {
  <#
  .SYNOPSIS
    Encodes a string to Base64 
  .DESCRIPTION
    Encodes a string to Base64. Support functionality that can be used to encode powershell's commands to be used with native -EncodedCommand option. i.e. powershell -EncodedCommand $encodedCommand
  .PARAMETER Command
    A powershell command to be encoded. 
  .EXAMPLE
    ConvertTo-Base64 'Write-Host Hello World!'
  #>

  param(
    [parameter(Mandatory=$true)]
    [string]$Command
  )

  $bytes = [System.Text.Encoding]::Unicode.GetBytes($Command)
  $encodedCommand = [Convert]::ToBase64String($bytes)
  return $encodedCommand
}