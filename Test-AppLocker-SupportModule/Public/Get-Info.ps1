Function Get-Info {
  <#
  .SYNOPSIS
    Provides a dictionary with system information
  .DESCRIPTION
    Provides a dictionary with system information
  .EXAMPLE
    Get-Info
  #>
  
  $assemblies = ("System")
  $source = 
@"
using System;

namespace HelperUtils
{
  public class GetInfo {
    public static string main() {
      return System.IO.Path.GetTempPath();
    }
  }
}
"@
  
  # Check if HelperUtils.GetInfo been loaded before and if not reference the assembly.
  if (-not ([System.Management.Automation.PSTypeName]'[HelperUtils.GetInfo]').Type) {
    Add-Type -ReferencedAssemblies $assemblies -TypeDefinition $source -Language CSharp
  } else {
    Write-Warning -Message "HelperUtils.GetInfo is already loaded!"
  }

  # Populate the dictionary with whatever information needed.
  $dict = @{}
  $dict.Add('TempPath', [HelperUtils.GetInfo]::main())

  $dict.Add('ExploitsPath', (Join-Path $Global:TestAppLockerRootPath 'Exploits'))
  $dict.Add('PayloadsPath', (Join-Path $Global:TestAppLockerRootPath 'Payloads'))
  $dict.Add('ScannersPath', (Join-Path $Global:TestAppLockerRootPath 'Scanners'))

  return $dict
}