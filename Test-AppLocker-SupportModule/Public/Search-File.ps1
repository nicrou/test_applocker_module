Function Search-File {
  <#
  .SYNOPSIS
    Simple search function to locate module files.
  .DESCRIPTION
    Simple search function to locate module files depending on their type (exploit, payload, scanner etc.).
  .PARAMETER FileName
    The name of the file you are looking for.
  .PARAMETER Type
    Type is a string that determines a category of files that by default have specific locations.
  .EXAMPLE
    Search-File -FileName CL_LoadAssembly.ps1 -Type Exploit
  #> 

  param(
    [parameter(Mandatory=$true)]
    [string]$FileName,
    [parameter(Mandatory=$false)]
    [string]$Type
  )

  if ($PSBoundParameters.ContainsKey('Type')) {
    $Type = (Get-Culture).TextInfo.ToTitleCase($Type) + 's'
    if ($Type -ne 'Exploits' -and $Type -ne 'Payloads' -and $Type -ne 'Scanners') {
      throw "[-] Invalid type: $Type"
    }

    $FilePath = Join-Path $Global:TestAppLockerRootPath $Type 
  } else {
    $FilePath = $Global:TestAppLockerRootPath 
  }

  if ($PSVersionTable.PSVersion -gt [Version]"3.0") {
    $FilePath = Get-ChildItem -Path $FilePath -Filter $FileName -Recurse -File | %{ $_.FullName } | Select-Object -first 1
  } else {
    $FilePath = Get-ChildItem -Path $FilePath -Filter $FileName -Recurse | Where-Object { $_.Attributes -ne "Directory"} | %{ $_.FullName } | Select-Object -first 1
  }

  if ($FilePath) {
    return $FilePath
  } else {
    throw "[-] Unable to locate $FileName"
  }
}